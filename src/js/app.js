/** ******** Your code here! *********** */
import { randomUserMock, additionalUsers } from './mock';

require('../css/app.css');

// randomizing
function randomizing(params) {
  return params[Math.floor(Math.random() * params.length)];
}
const fav = [true, false];
const courses = [
  'Mathematics',
  'Physics',
  'English',
  'Computer Science',
  'Dancing',
  'Chess',
  'Biology',
  'Chemistry',
  'Law',
  'Art',
  'Medicine',
  'Statistics',
];
const bgColors = ['#dface7', '#1f75cb', '#70DF6E', '#FFFF00'];
const notes = [
  'Old lady with a cats',
  'Young man with a hat',
  'Just a beautiful woman',
  'Rich old man',
];
function normalize() {
  // merging objects by name without getting repeats
  const userNames = randomUserMock.map((item) => [item.name.first, item.name.last]);
  const additionalUserNames = additionalUsers.map((item) => item.full_name.split(' '));
  for (let i = 0; i < userNames[i].length; i += 1) {
    if (userNames[i][i] === additionalUserNames[i][i]) {
      randomUserMock[i] = { ...randomUserMock[i], ...additionalUsers[i] };
      additionalUsers.splice(i, 1);
    }
  }
  const usersInfo = [...randomUserMock, ...additionalUsers];

  // adding fields
  usersInfo.map((x) => {
    const elem = x;
    elem.favorite = randomizing(fav);
    elem.course = randomizing(courses);
    elem.bg_color = randomizing(bgColors);
    elem.note = randomizing(notes);
    // correct id
    if (typeof elem.id !== 'string') {
      elem.id = elem.id.name + elem.id.value;
      elem.id = elem.id
        .replace(/[^a-zа-яі0-9\s]/gi, ' ')
        .split(' ')
        .join('')
        .toLowerCase();
    }
    Object.assign(elem, elem.location);
    Object.assign(elem, elem.name);
    Object.assign(elem, elem.dob);

    delete elem.location;
    delete elem.name;
    delete elem.dob;

    if (!elem.full_name) {
      elem.full_name = `${elem.first} ${elem.last}`;
    }
    if (!elem.b_day) {
      elem.b_day = elem.date;
    }
    delete elem.date;
    return elem;
  });

  return usersInfo;
}
const normalizedUsers = normalize();
// typeof el.age === "number" &&
function validation(arr) {
  return arr.filter(
    (el) => el.full_name !== undefined
      && el.full_name.charAt(0) === el.full_name.charAt(0).toUpperCase()
      && el.gender !== undefined
      && el.note.charAt(0) === el.note.charAt(0).toUpperCase()
      && el.state !== undefined
      && el.state.charAt(0) === el.state.charAt(0).toUpperCase()
      && el.city !== undefined
      && el.city.charAt(0) === el.city.charAt(0).toUpperCase()
      && el.country !== undefined
      && el.country.charAt(0) === el.country.charAt(0).toUpperCase(),
  );
}
const validatedUsers = validation(normalizedUsers);

// filtration

function filtration(arr, {
  gender, country, age, favorite,
}) {
  return arr.filter(
    (el) => (age ? age === el.age : true)
      && (gender ? gender === el.gender : true)
      && (country ? country === el.country : true)
      && (favorite ? favorite === el.favorite : true),
  );
}

// sorting

function sorting(arr, sortBy, direction) {
  switch (sortBy) {
    case 'age':
      return arr.sort((a, b) => (a.age > b.age ? direction : -direction));
    case 'name':
      return arr.sort((a, b) => (a.full_name > b.full_name ? direction : -direction));
    case 'speciality':
      return arr.sort((a, b) => (a.course > b.course ? direction : -direction));
    case 'b_day':
      return arr.sort((a, b) => (a.b_day > b.b_day ? direction : -direction));
    case 'country':
      return arr.sort((a, b) => (a.country > b.country ? direction : -direction));
    default:
      return 'missing parametr';
  }
}
// search

function search(arr, prop) {
  const result = [];
  arr.forEach((user) => {
    if (Object.values(user).includes(prop)) result.push(user);
  });
  return result;
}

function totalAgePercentage(arr, age) {
  const searchAge = arr.filter((el) => el.age < age);
  return Math.floor((searchAge.length / arr.length) * 100);
}
totalAgePercentage(validatedUsers, 60);

// DOM

/// / UserDisplay

const formUserCard = (user) => {
  const btn = document.createElement('button');
  const article = document.createElement('article');
  // favorite check, add star
  if (user.favorite === true) {
    article.innerHTML = '<div class="star"></div>';
  }
  article.insertAdjacentHTML(
    'afterbegin',
    `<div class="photo">
            <img class="personPhoto" alt="" src="${user.picture.large}">
        </div>
        <p class="name">${user.full_name}</p>
        <p class="speciality">${user.course}</p>
        <p class="country">${user.country}</p>  `,
  );
  btn.classList.add('btn-active');
  btn.appendChild(article);
  return btn;
};
const formCardBody = (users) => {
  const teachers = document.querySelector('.teachers');
  teachers.innerHTML = '';
  users.forEach((user) => {
    const card = formUserCard(user);
    teachers.appendChild(card);
  });
  return teachers;
};

const displayCard = (arr) => {
  formCardBody(arr);
};
displayCard(validatedUsers);

/// /MODAL

const formModalWindow = (data) => {
  const teachersInfo = document.createElement('div');
  teachersInfo.classList.add('all_teachers');

  teachersInfo.innerHTML = `<div class="modal_back" id="${data.last}">
      <div class="modal_teacher_info">
          <div class="teacher_info">
              <div class="teacher_info-head">
              <span>Teacher info</span>
              <button id="btn-close"><i class="fa-solid fa-xmark"></i></button>
              </div>
              <div class="teacher_info-main">
              <div class="teachers_photo">
                  <img src="${data.picture.large}" alt="" />
              </div>
              <div id="teachers_star" class="teachers_star"></div>
              <div class="teachers_data">
                  <span class="teachers_name">${data.full_name}</span>
                  <span class="teachers_speciality">${data.course}</span>
                  <span class="teachers_data-second">${data.country}, ${data.city}</span>
                  <span class="teachers_data-second">${data.age}, ${data.gender}</span>
                  <span class="teachers_data-second"><a>${data.email}</a></span>
                  <span class="teachers_data-second">${data.phone}</span>
              </div>
              <div class="teachers_notes">
                  <p>${data.note}</p>
              </div>
              <div class="teachers_map"><a>toggle map</a></div>
              </div>
          </div>
      </div>
  </div>`;

  return teachersInfo;
};

const formModalBody = (arr) => {
  const teachersInfo = document.querySelector('.all_teachers_info');
  teachersInfo.innerHTML = '';
  document.querySelectorAll('.btn-active').forEach((item) => {
    item.addEventListener('click', () => {
      arr.forEach((user) => {
        if (user.last === item.id) {
          teachersInfo.appendChild(formModalWindow(user));
        }
      });
    });
  });

  return teachersInfo;
};
formModalBody(validatedUsers);

/// /Filter
function filterUsers(params) {
  const userAge = document.querySelector('#Age');
  const userRegion = document.querySelector('#Region');
  const userSex = document.querySelector('#Sex');
  const userOnlyfavorites = document.querySelector('#Onlyfavorites');

  const filterParams = {};
  const filterCardBody = () => formCardBody(filtration(params, filterParams));

  userSex.addEventListener('change', () => {
    filterParams.gender = userSex.value;
    filterCardBody();
  });
  userRegion.addEventListener('change', () => {
    filterParams.country = userRegion.value;
    filterCardBody();
  });
  userAge.addEventListener('change', () => {
    filterParams.age = Number(userAge.value);
    filterCardBody();
  });
  userOnlyfavorites.addEventListener('change', () => {
    filterParams.favorite = userOnlyfavorites.checked;
    filterCardBody();
  });
}
filterUsers(validatedUsers);
/// /statiscicsTable

function statiscicsTable(params) {
  /* eslint-disable */
  const formRow = ({ full_name, course, age, b_day, country }) => {
    const tr = document.createElement("tr");
    const generatedHTML = [full_name, course, age, b_day, country]
      .map((prop) => `<td>${prop}</td>`)
      .join("");
    /* eslint-enable */

    tr.innerHTML = generatedHTML;
    return tr;
  };

  const formBody = (users) => {
    const tbody = document.querySelector('tbody');
    tbody.innerHTML = '';
    users.forEach((user) => {
      const row = formRow(user);
      tbody.appendChild(row);
    });
    return tbody;
  };

  const displayStatistics = (arr) => {
    const sortName = document.querySelector('#sort_name');
    const sortSpeciality = document.querySelector('#sort_speciality');
    const sortAge = document.querySelector('#sort_age');
    const sortBday = document.querySelector('#sort_b_day');
    const sortCountry = document.querySelector('#sort_country');
    formBody(arr);

    sortName.addEventListener('click', () => {
      const sortedUsers = sorting(arr, 'name', 1);
      formBody(sortedUsers);
    });
    sortSpeciality.addEventListener('click', () => {
      const sortedUsers = sorting(arr, 'speciality', 1);
      formBody(sortedUsers);
    });
    sortAge.addEventListener('click', () => {
      const sortedUsers = sorting(arr, 'age', 1);
      formBody(sortedUsers);
    });
    sortBday.addEventListener('click', () => {
      const sortedUsers = sorting(arr, 'b_day', 1);
      formBody(sortedUsers);
    });
    sortCountry.addEventListener('click', () => {
      const sortedUsers = sorting(arr, 'country', 1);
      formBody(sortedUsers);
    });
  };

  displayStatistics(params);
}
statiscicsTable(validatedUsers);

function usersearch(arr) {
  const searchBody = document.querySelector('.search_result');
  const topSection = document.querySelector('.top');
  const searchBtn = document.querySelector('.search_btn');
  const searchInput = document.querySelector('.search_input');

  const img = document.createElement('img');
  const name = document.createElement('p');
  const age = document.createElement('p');
  const note = document.createElement('p');

  searchBtn.addEventListener('click', () => {
    console.log(search(arr, searchInput.value));
    search(arr, searchInput.value).forEach((user) => {
      img.src = `${user.picture.large}`;
      name.innerHTML = `${user.full_name}`;
      age.innerHTML = `${user.age}`;
      note.innerHTML = `${user.note}`;
    });
  });
  searchBody.appendChild(note);
  topSection.appendChild(img);
  topSection.appendChild(name);
  topSection.appendChild(age);
}
usersearch(validatedUsers);

function addTeachers() {
  const btn = document.querySelector('#send_form');
  const form = document.forms.add_user;
  const result = [];
  btn.addEventListener('click', () => {
    result.push({
      gender: form.sex.value,
      full_name: form.name.value,
      city: form.City.value,
      country: form.countries.value,
      b_day: form.Date.value,
      course: form.Speciality.value,
      bg_color: form.Color.value,
      note: form.Notes.value,
      favorite: false,
      picture: {
        large: 'https://randomuser.me/api/portraits/men/91.jpg',
      },
    });
    const formCardBodyNew = (users) => {
      const teachers = document.querySelector('.teachers');
      users.forEach((user) => {
        const card = formUserCard(user);
        teachers.appendChild(card);
      });
      return teachers;
    };
    formCardBodyNew(result);
  });
}
addTeachers();
